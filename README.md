 # Simple API test

To run the project do in terminal:
 ```
docker-compose up
 ```

 Import database scripts (tables.sql for database and tables, data.sql for data) with mysql dump or connect to the database with a client (heidi SLQ) 
 Grant privilege to new created database accordling to your .env file, adjust also property values in database.php file

 From the browser connect to [](http://localhost/api.php) include query params.

 Available params:

 * node_id     int
 * language   string , option  italian or english
 * search      string
 * page_num    int
 * page_size   int

 To test the API use a client like PostMan or Thunder client if in vscode.
