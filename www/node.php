<?php

/**
  * Description for the class
  * @property int $node_id Requested parent node_id
  * @property string $language Requested node language
  * @property string $search Optional nodeName search string
  * @property int $page_num Optional page number request for pagination
  * @property int $page_size Oprional page size request for pagination
  * @property array $errors Array for error messages response
  * @property object $conn database connection
  * @property int $offset Offset for query limit
  * @property int $row_count Row count for query limit
  * @property array $language Allowed languages
  */
class Node{
  
    public $node_id;
    public $language;
    public $search;
    public $page_num;
    public $page_size;
    public $errors = array();

    private $conn;
    private $offset;
    private $row_count;
    private $languages = array(
        'italian' => 1,
        'english' => 2
    );

    /**
    * constructor with $db as database connection
    */
    public function __construct($db){
        $this->conn = $db;
    }

    /**
    * prepare query params, set default value, calculate data for pagination 
    * and invoke checkErrors() for validation
    * @return boolean
    */
    public function prepareQueryParams(){

        $this->page_num = is_null($this->page_num) ? 0 : $this->page_num;;
        $this->page_size = is_null($this->page_size) ? 100 : $this->page_size;
        $this->language = $this->languages[$this->language];

        if (!$this->checkErrors()){
            return false;
        }

        $this->search = htmlspecialchars(strip_tags($this->search));
        $this->offset = $this->page_num * $this->page_size;
        $this->row_count = (1 + $this->page_num) * $this->page_size;

        return true;
    }

    /**
    * query the database
    * @return array
    */
    public function getNodeChildren(){
        $query = "SELECT c.idNode, ntn.nodeName,
                        (
                        SELECT COUNT(cc.idNode)
                        FROM node_tree AS cc JOIN node_tree AS pp
                        WHERE cc.iLeft BETWEEN pp.iLeft AND pp.iRight
                            AND cc.level = pp.level + 1
                            AND pp.level = c.level
                            AND pp.idNode = c.idNode
                        
                        ) AS children
                  FROM node_tree as c 
                  JOIN node_tree as p
                  JOIN node_tree_names AS ntn ON c.idNode = ntn.idNode
                  WHERE c.iLeft BETWEEN p.iLeft AND p.iRight
                        AND c.level = p.level + 1
                        AND p.idNode = :node_id
                        AND ntn.language = :language";
        if($this->search){
            $query.=" AND ntn.nodeName LIKE CONCAT('%', :search, '%')";
        }
        $query.=" LIMIT :offset, :row_count";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':node_id', $this->node_id, PDO::PARAM_INT);
        $stmt->bindParam(':language', $this->language, PDO::PARAM_INT);
        $stmt->bindParam(':offset', $this->offset, PDO::PARAM_INT);
        $stmt->bindParam(':row_count', $this->row_count ,PDO::PARAM_INT);
        if($this->search){
            $stmt->bindParam(':search', $this->search, PDO::PARAM_STR);
        }

        $stmt->execute();
    
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    /**
    * check if required node exist for all the required field (node_id and language)
    * @return boolean
    */
    private function checkNodeExists(){
        $query = "SELECT IF(COUNT(*) > 0, 1, 0) AS node FROM node_tree_names WHERE idNode = :node_id AND language = :language";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':node_id', $this->node_id, PDO::PARAM_INT);
        $stmt->bindParam(':language', $this->language, PDO::PARAM_INT);
        $stmt->execute();
        return  $stmt->fetch(PDO::FETCH_ASSOC)['node'];
    }
    /**
    * Check if param has error and push error message to errors array
    * @return boolean
    */
    private function checkErrors(){
        if (!$this->node_id || !$this->language){
            array_push($this->errors,"Missing mandatory params");
        }elseif (!$this->checkNodeExists()){
            array_push($this->errors,"Invalid Node Id");
        }
        if($this->page_num === false){
            array_push($this->errors,"Invalid page number request");
        }
        if($this->page_size === false){
            array_push($this->errors,"Invalid page size request");
        }

        if(count($this->errors) > 0){
            return false;
        }        
        return true;
    }    
}
?>