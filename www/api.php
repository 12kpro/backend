<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object file
include_once 'database.php';
include_once 'node.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare product object
$node = new Node($db);
$response = array();

// build request url 
$url = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on' ? 'https' : 'http' ) . '://' .  $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

//Verify that the url is valid has the query params and the method is GET else return error
if(filter_var($url,FILTER_VALIDATE_URL,FILTER_FLAG_QUERY_REQUIRED) && $_SERVER['REQUEST_METHOD'] === 'GET'){

        // validate query params check it correspond to the required values
        $node->node_id   = filter_input(INPUT_GET, 'node_id',   FILTER_VALIDATE_INT,['options' => array('min_range' => 1)]);
        $node->language  = filter_input(INPUT_GET, 'language',  FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH);
        $node->search    = filter_input(INPUT_GET, 'search',    FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH);
        $node->page_num  = filter_input(INPUT_GET, 'page_num',  FILTER_VALIDATE_INT,['options' => array('min_range' => 0)]);
        $node->page_size = filter_input(INPUT_GET, 'page_size', FILTER_VALIDATE_INT,['options' => array('min_range' => 0, 'max_range' => 1000)]);

        // Prepare query params and detect errors, if no error detected return nodes else return errors object
        if ($node->prepareQueryParams()) {
            $response['nodes'] = $node->getNodeChildren();
        } else {
            $response['nodes'] = [];
            $response['errors'] = $node->errors;
        }
        
        // set response code - 200 OK
        http_response_code(200);
        echo json_encode($response);
}else{
        http_response_code(404);
        $response['error'] = "Invalid URL or missing query string";
        echo json_encode($response);
}
?>