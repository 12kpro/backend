FROM php:7.4-apache

RUN docker-php-ext-install mysqli 
RUN docker-php-ext-install pdo_mysql
# Install Xdebug
#RUN pecl install -f xdebug
#RUN echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini;
