-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versione server:              8.0.25 - MySQL Community Server - GPL
-- S.O. server:                  Linux
-- HeidiSQL Versione:            11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dump dei dati della tabella test.node_tree: ~12 rows (circa)
/*!40000 ALTER TABLE `node_tree` DISABLE KEYS */;
INSERT INTO `node_tree` (`idNode`, `level`, `iLeft`, `iRight`) VALUES
	(1, 2, 2, 3),
	(2, 2, 4, 5),
	(3, 2, 6, 7),
	(4, 2, 8, 9),
	(5, 1, 1, 24),
	(6, 2, 10, 11),
	(7, 2, 12, 19),
	(8, 3, 15, 16),
	(9, 3, 17, 18),
	(10, 2, 20, 21),
	(11, 3, 13, 14),
	(12, 2, 22, 23);
/*!40000 ALTER TABLE `node_tree` ENABLE KEYS */;

-- Dump dei dati della tabella test.node_tree_names: ~24 rows (circa)
/*!40000 ALTER TABLE `node_tree_names` DISABLE KEYS */;
INSERT INTO `node_tree_names` (`idNode`, `language`, `nodeName`) VALUES
	(1, 'english', 'Marketing'),
	(1, 'italian', 'Marketing'),
	(2, 'english', 'Helpdesk'),
	(2, 'italian', 'Supporto tecnico'),
	(3, 'english', 'Managers'),
	(3, 'italian', 'Managers'),
	(4, 'english', 'Customer Account'),
	(4, 'italian', 'Assistenza Cliente'),
	(5, 'english', 'Docebo'),
	(5, 'italian', 'Docebo'),
	(6, 'english', 'Accounting'),
	(6, 'italian', 'Amministrazione'),
	(7, 'english', 'Sales'),
	(7, 'italian', 'Supporto Vendite'),
	(8, 'english', 'Italy'),
	(8, 'italian', 'Italia'),
	(9, 'english', 'Europe'),
	(9, 'italian', 'Europa'),
	(10, 'english', 'Developers'),
	(10, 'italian', 'Sviluppatori'),
	(11, 'english', 'North America'),
	(11, 'italian', 'Nord America'),
	(12, 'english', 'Quality Assurance'),
	(12, 'italian', 'Controllo Qualità');
/*!40000 ALTER TABLE `node_tree_names` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
